Ansible Role: artisan-control
=========

A collection of useful artisan commands.


Role Variables
--------------

path_to_site_current: <specify the full path to your web app>
role_become:
role_become_user:

Default Variables
--------------

role_become: true
role_become_user: app

Dependencies
------------

Laravel (Artisan Console) on the server that you connect to.

Example Playbook
----------------

    - hosts: servers
      tasks:
        - include_role:
            name: artisan-control
            tasks_from: down
          vars:
            path_to_site_current: /var/www/sites/semesteravgift.uib.no/current/

License
-------

MIT

